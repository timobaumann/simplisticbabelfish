package synthesis;

import java.util.Collection;
import java.util.List;

import inpro.apps.SimpleMonitor;
import inpro.audio.DispatchStream;
import inpro.incremental.IUModule;
import inpro.incremental.processor.SynthesisModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.HesitationIU;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.PhraseIU;

public class SynthesisRunner extends IUModule {
	
	/** audio stream that we dispatch to */
	DispatchStream dispatcher;
	
	/** set up the synthesis runner */
	public SynthesisRunner() {
		// get an output object that plays back on the speakers/headphone
		dispatcher = SimpleMonitor.setupDispatcher();
		// setup the synthesis module to play audio to this dispatcher
		SynthesisModule synth = new SynthesisModule(dispatcher);
		this.addListener(synth);
	}
	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		// nothing to do here, we do not plan to consume IUs but only to produce IUs for the time being.
	}

	public static void main(String[] args) {
		// setup
		SynthesisRunner synRunner = new SynthesisRunner();

		// construct a first and very simple phrase: 
		PhraseIU phrase1 = new PhraseIU("Hallo Welt");
		// add the phrase to our output buffer
		synRunner.rightBuffer.addToBuffer(phrase1);
		// trigger notification of the listeners (currently only the synthesis module)
		synRunner.notifyListeners();
		
		// make sure the program doesn't exit before synthesis has completed.
		synRunner.dispatcher.waitUntilDone();
	}

}
